# vue-build-target-test

I wanted to compare the build size of a Vue.js component as a library vs a web component. I started with [Javi's button on Storybook](https://5fd3c2ae129fb60008195486--inspiring-turing-4fa0ff.netlify.app/?path=/story/buttons--primary-button) as an example.  

## See for yourself 

The [Vue CLI has different build targets](https://cli.vuejs.org/guide/build-targets.html#library). 

To build for a library, run: 

```sh
yarn vue-cli-service build --target lib
```

At the time of writing, the output is something like: 

```sh
  File                      Size     Gzipped

  dist/vue-build-target-    3.74 KiB 1.68 KiB
  test.umd.min.js
  dist/vue-build-target-    13.74    4.12 KiB
  test.umd.js               KiB
  dist/vue-build-target-    13.34    4.02 KiB
  test.common.js            KiB
```

To build to a web component, run: 

```sh
yarn vue-cli-service build --target wc --name cta-button src/components/Button.vue
```

At the time of writing, the output is something like: 

```sh
  File                      Size                              Gzipped

  dist/cta-button.min.js    7.18 KiB                          3.15 KiB
  dist/cta-button.js        24.11 KiB                         7.42 KiB
```


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
